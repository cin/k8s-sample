package de.ingenhaag;

import io.micronaut.context.event.ApplicationEventListener;
import io.micronaut.runtime.context.scope.refresh.RefreshEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Singleton;

@Singleton
public class RefreshEventListener implements ApplicationEventListener<RefreshEvent> {

  private static final Logger LOG = LoggerFactory.getLogger(RefreshEventListener.class);

  @Override
  public void onApplicationEvent(RefreshEvent event) {
    LOG.info(event.toString());
  }
}
