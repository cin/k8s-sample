package de.ingenhaag;


import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;

import javax.inject.Inject;
import java.util.Map;

@Controller("/api")
public class ConfigController {

  @Inject
  private ConfigService configService;

  @Get("/config")
  public HttpResponse<Map> getConfig() {
    Map response = Map.of("configa", configService.getConfigA(), "configb", configService.getConfigB());

    return HttpResponse.ok(
        response);
  }

  @Get("/date")
  public HttpResponse<Map> getDate() {
    Map response = Map.of("date", configService.getForecast());
    return HttpResponse.ok(response);
  }
}
