package de.ingenhaag;

import io.micronaut.context.annotation.Value;
import io.micronaut.runtime.context.scope.Refreshable;

import javax.annotation.PostConstruct;
import java.text.SimpleDateFormat;
import java.util.Date;

@Refreshable
public class ConfigService {
  private String forecast;

  @Value("${k8ssample.configa}")
  private String configA;

  @Value("${k8ssample.configb}")
  private String configB;

  @PostConstruct
  public void init() {
    forecast = "Scattered Clouds " + new SimpleDateFormat("dd/MMM/yy HH:mm:ss.SSS").format(new
        Date());
  }

  public String getForecast() {
    return forecast;
  }

  public void setForecast(String forecast) {
    this.forecast = forecast;
  }

  public String getConfigA() {
    return configA;
  }

  public void setConfigA(String configA) {
    this.configA = configA;
  }

  public String getConfigB() {
    return configB;
  }

  public void setConfigB(String configB) {
    this.configB = configB;
  }
}